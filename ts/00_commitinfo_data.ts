/**
 * autocreated commitinfo by @pushrocks/commitinfo
 */
export const commitinfo = {
  name: '@pushrocks/smartenv',
  version: '5.0.6',
  description: 'store things about your environment and let them travel across modules'
}
