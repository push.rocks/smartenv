import { tap, expect } from '@pushrocks/tapbundle';
import * as smartenv from '../ts/index.js';

let testEnv: smartenv.Smartenv;

tap.test('should print env', async () => {
  testEnv = new smartenv.Smartenv();
});

tap.test('should print a overview to console', async () => {
  testEnv.printEnv();
});

tap.test('should get os', async () => {
  const resultMac = await testEnv.isMacAsync();
  const resultLinux = await testEnv.isLinuxAsync();
  const resultWindows = await testEnv.isWindowsAsync();
  const osModule = await import('os');
  if (resultMac) {
    expect(osModule.platform()).toEqual('darwin');
    console.log('platform is Mac!');
  } else if (resultLinux) {
    expect(osModule.platform()).toEqual('linux');
    console.log('platform is Linux!');
  } else {
    expect(osModule.platform()).toEqual('win32');
    console.log('platform is Windows!');
  }
});

tap.test('should state wether we are in CI', async () => {
  if (process.env.CI) {
    expect(testEnv.isCI).toBeTrue();
  }
});

tap.start();
